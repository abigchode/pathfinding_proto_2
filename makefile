main: $(wildcard *.cpp) $(wildcard *.hpp)
	g++ -g -std=c++14 -O3 $(wildcard *.cpp) -o $@
clean:
	rm -f main