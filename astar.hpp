#ifndef ASTAR_H
#define ASTAR_H

#include <iostream>
#include <vector>
#include <functional>
#include <unordered_map>
#include <queue>
#include <boost/function_output_iterator.hpp>

template <typename TSearchItem>
struct Node
{
    const TSearchItem* item;
    const Node<TSearchItem>* parent;
    float g;
    float h;
    
    Node(const TSearchItem* item, const Node<TSearchItem>* parent, float g, float h) : item(item), parent(parent), g(g), h(h)
    {
    }
    
    float cost() const
    {
        return g + h;
    }
};

template <typename TSearchItem>
class Compare
{
public:
    bool operator()(const Node<TSearchItem>* a, const Node<TSearchItem>* b)
    {
        return a->cost() > b->cost();
    }
};

template <typename TSearchItem, typename FEndReached, typename FMoveCost, typename FHeuristic, typename FSurroundingNodes, typename TOutputIterator>
void astar_search(const TSearchItem& start, const FEndReached& endReached, const FMoveCost& getMoveCost, const FHeuristic& getHeuristic, const FSurroundingNodes& getSurroundingNodes, TOutputIterator output)
{
    auto searchedNodes = std::unordered_map<const TSearchItem*, Node<TSearchItem>>();
    auto frontier = std::priority_queue<const Node<TSearchItem>*, std::vector<const Node<TSearchItem>*>, const Compare<TSearchItem>>();
    
    auto startNode = Node<TSearchItem>(&start, nullptr, (float)0, getHeuristic(start));
    searchedNodes.emplace(&start, startNode);
    frontier.push(&startNode);
    
    while(!frontier.empty())
    {
        auto current = frontier.top();
        frontier.pop();
        
        if(endReached(*current->item))
        {
            //std::cout << searchedNodes.size() << std::endl;
            //std::cout << frontier.size() << std::endl;
            while(current != 0)
            {
                *output++ = current->item;
                current = current->parent;
            }
            //std::cout << "path found" << std::endl;
            return;
        }
        
        getSurroundingNodes(*current->item, boost::make_function_output_iterator([&] (const TSearchItem* next)
        {
            const auto& searchedNode = searchedNodes.find(next);
            auto newCost = current->g + getMoveCost(*current->item, *next);
            
            if(searchedNode == searchedNodes.end())
            {
                const auto& newNode = Node<TSearchItem>(next, current, newCost, getHeuristic(*next));
                searchedNodes.emplace(next, newNode);
                frontier.push(&searchedNodes.at(next));
            }
            else if(newCost < searchedNode->second.g)
            {
                searchedNode->second.parent = current;
                searchedNode->second.g = newCost;
            }
        }));
    }
}

#endif