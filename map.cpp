#include "map.hpp"

bool Map::inBounds(int x, int y)
{
    return x >= 0 && y >= 0 && x < width && y < height;
}

Tile* Map::getTile(int x, int y) 
{
    if(inBounds(x, y)) 
    {
        return &_tiles.at(x * height + y);
    }
    else 
    {
        return 0;
    }
}