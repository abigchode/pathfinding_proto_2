#ifndef TILE_H
#define TILE_H

class Tile
{  
public:
    int x, y;
    bool passable;
    
    Tile(int x, int y) : x(x), y(y) 
    {
        this->passable = true;
    }
};

#endif