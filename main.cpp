#include <iostream>
#include <cmath>
#include <chrono>

#include "map.hpp"
#include "tile.hpp"
#include "astar.hpp"

int main(int argc, char **argv) 
{
    Map map(256, 256);
    float sqrt2 = sqrt(2);
    auto t1 = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < 10; i++)
    {
        auto start = map.getTile(0, 255);
        auto end = map.getTile(255, 0);
        std::vector<const Tile*> path;
        astar_search(
            *start,
            //[&](const Tile& t) { return t.x == end->x && t.y == end->y; },
            [&](const Tile& t) { return false; },
            [&](const Tile& a, const Tile& b) { return a.x - b.x == 0 || a.y - b.y == 0 ? (float)1 : sqrt2; },
            [&](const Tile& t) { return (float)abs(t.x - end->x) + (float)abs(t.y - end->y); },
            [&](const Tile& t, auto output) { map.getSurrounding(t, output); },
            std::back_inserter(path)
        );
    }
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << std::endl;
    
    /*for(auto tile : path)
    {
        std::cout << "tile x: " << tile->x << ", tile y: " << tile->y << std::endl;
    }
    std::cout << "done" << std::endl;*/
    
    return 0;
}