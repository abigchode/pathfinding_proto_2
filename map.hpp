#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <vector>
#include <boost/optional.hpp>

#include "tile.hpp"

class Map
{
    std::vector<Tile> _tiles;

public:
    int width, height;

    Map(int width, int height) : width(width), height(height) 
    {
        for(int i = 0; i < width; i++) 
        {
            for(int j = 0; j < height; j++) 
            {
                _tiles.push_back(Tile(i, j));
            }
        }
    }
    
    bool inBounds(int x, int y);

    Tile* getTile(int x, int y);
    
    template <typename TOutputIterator>
    void getSurrounding(const Tile& t, TOutputIterator output)
    {
        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j <= 1; j++)
            {
                if(i != 0 || j != 0)
                {
                    int x = t.x + i;
                    int y = t.y + j;
                    if(inBounds(x, y))
                    {
                        const auto& tile = _tiles.at(x * height + y);
                        if(tile.passable)
                        {
                            *output++ = &tile;
                        }
                    }
                }
            }
        }
    }
};

#endif